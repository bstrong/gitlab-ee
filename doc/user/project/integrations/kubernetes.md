# GitLab Kubernetes / OpenShift integration

GitLab can be configured to interact with Kubernetes, or other systems using the
Kubernetes API (such as OpenShift).

Each project can be configured to connect to a different Kubernetes cluster, see
the [configuration](#configuration) section.

If you have a single cluster that you want to use for all your projects,
you can pre-fill the settings page with a default template. To configure the
template, see the [Services Templates](services_templates.md) document.

## Configuration

Navigate to the [Integrations page](project_services.md#accessing-the-project-services)
of your project and select the **Kubernetes** service to configure it. Fill in
all the needed parameters, check the "Active" checkbox and hit **Save changes**
for the changes to take effect.

![Kubernetes configuration settings](img/kubernetes_configuration.png)

The Kubernetes service takes the following parameters:

- **API URL** -
  It's the URL that GitLab uses to access the Kubernetes API. Kubernetes
  exposes several APIs, we want the "base" URL that is common to all of them,
  e.g., `https://kubernetes.example.com` rather than `https://kubernetes.example.com/api/v1`.
- **CA certificate** (optional) -
  If the API is using a self-signed TLS certificate, you'll also need to include
  the `ca.crt` contents here.
- **Project namespace** (optional) - The following apply:
  - By default you don't have to fill it in; by leaving it blank, GitLab will
    create one for you.
  - Each project should have a unique namespace.
  - The project namespace is not necessarily the namespace of the secret, if
    you're using a secret with broader permissions, like the secret from `default`.
  - You should **not** use `default` as the project namespace.
  - If you or someone created a secret specifically for the project, usually
    with limited permissions, the secret's namespace and project namespace may
    be the same.
- **Token** -
  GitLab authenticates against Kubernetes using service tokens, which are
  scoped to a particular `namespace`. If you don't have a service token yet,
  you can follow the
  [Kubernetes documentation](https://kubernetes.io/docs/tasks/configure-pod-container/configure-service-account/)
  to create one. You can also view or create service tokens in the
  [Kubernetes dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/#config)
  (under **Config > Secrets**).

[namespace]: https://kubernetes.io/docs/user-guide/namespaces/

## Deployment variables

The Kubernetes service exposes following
[deployment variables](../../../ci/variables/README.md#deployment-variables) in the
GitLab CI build environment:

- `KUBE_URL` - equal to the API URL
- `KUBE_TOKEN`
- `KUBE_NAMESPACE` - The Kubernetes namespace is auto-generated if not specified.
  The default value is `<project_name>-<project_id>`. You can overwrite it to
  use different one if needed, otherwise the `KUBE_NAMESPACE` variable will
  receive the default value.
- `KUBE_CA_PEM_FILE` - only present if a custom CA bundle was specified. Path
  to a file containing PEM data.
- `KUBE_CA_PEM` (deprecated)- only if a custom CA bundle was specified. Raw PEM data.
- `KUBECONFIG` - Path to a file containing kubeconfig for this deployment. CA bundle would be embedded if specified.

## Web terminals

NOTE: **Note:**
Added in GitLab 8.15. You must be the project owner or have `master` permissions
to use terminals. Support is currently limited to the first container in the
first pod of your environment.

When enabled, the Kubernetes service adds [web terminal](../../../ci/environments.md#web-terminals)
support to your [environments](../../../ci/environments.md). This is based on the `exec` functionality found in
Docker and Kubernetes, so you get a new shell session within your existing
containers. To use this integration, you should deploy to Kubernetes using
the deployment variables above, ensuring any pods you create are labelled with
`app=$CI_ENVIRONMENT_SLUG`. GitLab will do the rest!
